package cs102;

public abstract class Employee implements Payable {
    private String firstName;
    private String lastName;
    private int socialSecurityNumber;

    public Employee(String firstName, String lastName, int socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public int getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }

    public String toString() {
        return this.firstName + " " + this.lastName + "\n"
                + "Social Security Number: " + this.socialSecurityNumber;
    }
}
