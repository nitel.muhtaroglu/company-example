package cs102;

public class Invoice implements Payable {
    private int quantity;
    private double pricePerItem;

    public Invoice(int quantity, double pricePerItem) {
        this.quantity = quantity;
        this.pricePerItem = pricePerItem;
    }

    public double getPayableAmount() {
        return this.quantity * this.pricePerItem;
    }

    public String toString() {
        return super.toString() + " Quantity=" + this.quantity
                + ", Price per item=" + this.pricePerItem
                + ", Total price=" + this.getPayableAmount();
    }
}
