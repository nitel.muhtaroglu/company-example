package cs102;

public final class HourlyEmployee extends Employee {
    private int hours;
    private double rate;

    public HourlyEmployee(String firstName, String lastName, int socialSecurityNumber, int hours, double rate) {
        super(firstName, lastName, socialSecurityNumber);
        this.hours = hours;
        this.rate = rate;
    }

    public double getPayableAmount() {
        if (hours > 40) {
            return 40 * this.rate + 1.5 * (this.hours - 40) * this.rate;
        } else {
            return this.hours * this.rate;
        }
    }

    public String toString() {
        return "Hourly Employee: " + super.toString()
                + "\nHourly Wage=" + this.rate + ", Hours Worked=" + this.hours;
    }
}
