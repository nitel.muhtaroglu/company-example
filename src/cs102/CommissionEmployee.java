package cs102;

public class CommissionEmployee extends Employee {
    private double commissionRate;
    private double grossSales;

    public CommissionEmployee(String firstName, String lastName, int socialSecurityNumber, double commissionRate,
                              double grossSales) {
        super(firstName, lastName, socialSecurityNumber);
        this.commissionRate = commissionRate;
        this.grossSales = grossSales;
    }

    public double getPayableAmount() {
        return this.grossSales * this.commissionRate;
    }

    public String toString() {
        return "Commission Employee: " + super.toString()
                + "\nGross Sales=" + this.grossSales
                + "\nCommission Rate=" + this.commissionRate;
    }
}
