package cs102;

public interface Payable {
    double getPayableAmount();
}
