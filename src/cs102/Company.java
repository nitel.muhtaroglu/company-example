package cs102;

import java.util.ArrayList;

public class Company {
    public static void main(String[] args) {
        ArrayList<Payable> payables = new ArrayList<Payable>();
        payables.add(new Invoice(10, 3.5));
        payables.add(new SalariedEmployee("Mary", "Jane", 1234, 1000));

        double total = 0.;
        for (Payable payable : payables) {
            System.out.println(payable);
            total += payable.getPayableAmount();
        }

        System.out.println("Total = " + total);
    }
}
