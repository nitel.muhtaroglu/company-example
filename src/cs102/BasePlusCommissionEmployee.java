package cs102;

public class BasePlusCommissionEmployee extends CommissionEmployee {
    private double baseSalary;

    public BasePlusCommissionEmployee(String firstName, String lastName, int socialSecurityNumber,
                                      double commissionRate, double grossSales, double baseSalary) {
        super(firstName, lastName, socialSecurityNumber, commissionRate, grossSales);
        this.baseSalary = baseSalary;
    }

    public double getPayableAmount() {
        return super.getPayableAmount() + this.baseSalary;
    }

    public String toString() {
        return "Base salaried " + super.toString()
                + "\nBase salary=" + this.baseSalary;
    }

    public double getBaseSalary() {
        return this.baseSalary;
    }

    public void setBaseSalary(double baseSalary) {
        this.baseSalary = baseSalary;
    }
}
