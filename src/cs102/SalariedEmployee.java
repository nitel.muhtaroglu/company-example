package cs102;

public final class SalariedEmployee extends Employee {
    private double weeklySalary;

    public SalariedEmployee(String firstName, String lastName, int socialSecurityNumber, double weeklySalary) {
        super(firstName, lastName, socialSecurityNumber);
        this.weeklySalary = weeklySalary;
    }

    public double getPayableAmount() {
        return this.weeklySalary;
    }

    public String toString() {
        return "Salaried employee: " + super.toString()
                + "\nWeekly Salary: " + this.weeklySalary;
    }
}
